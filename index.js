const functions = require("firebase-functions");
const app = require("express")();

const auth = require("./util/auth");

const {
    getAllBudgets,
    getABudget,
    addABudget,
    deleteABudget,
    editABudget,
} = require("./APIs/budgets");

const { login, signUp, getUser, updateUser } = require("./APIs/users");

// Budgets
app.get("/budgets", auth, getAllBudgets);
app.get("/budget/:bugetId", auth, getABudget);
app.post("/budgets/add", auth, addABudget);
app.delete("/budgets/delete/:budgetId", auth, deleteABudget);
app.put("/budgets/edit/:budgetId", auth, editABudget);

// Users
app.post("/login", login);
app.post("/signUp", signUp);
app.get("/user", auth, getUser);
app.post("/user", auth, updateUser);

exports.api = functions.https.onRequest(app);
