const { admin, db } = require("../util/admin");
const config = require("../util/config");

const firebase = require("firebase");

firebase.initializeApp(config);

const { validateLogin, validateSignUp } = require("../util/validators");

exports.login = (request, response) => {
    const user = {
        email: request.body.email,
        password: request.body.password,
    };

    const { valid, errors } = validateLogin(user);
    if (!valid) {
        return response.status(400).json(errors);
    }

    firebase
        .auth()
        .signInWithEmailAndPassword(user.email, user.password)
        .then((data) => {
            return data.user.getIdToken();
        })
        .then((token) => {
            return response.json({ token });
        })
        .catch((error) => {
            console.error(error);
            return response.status(403).json({
                general: "Your credentials were not correct, please try again!",
            });
        });
};

exports.signUp = (request, response) => {
    const newUser = {
        email: request.body.email,
        password: request.body.password,
        confirmPassword: request.body.confirmPassword,
    };

    const { valid, errors } = validateSignUp(newUser);

    if (!valid) {
        return response.status(400).json(errors);
    }

    let token, id;
    db.doc(`/users/${newUser.email}`)
        .get()
        .then((doc) => {
            if (doc.exists) {
                return response
                    .status(400)
                    .json({ email: "This email has already been registered!" });
            } else {
                return firebase
                    .auth()
                    .createUserWithEmailAndPassword(
                        newUser.email,
                        newUser.password,
                    );
            }
        })
        .then((data) => {
            id = data.user.uid;
            return data.user.getIdToken();
        })
        .then((idToken) => {
            token = idToken;
            const credentials = {
                email: newUser.email,
            };

            return db.doc(`/users/${newUser.email}`).set(credentials);
        })
        .then(() => {
            return response.status(201).json({ token });
        })
        .catch((err) => {
            console.error(err);
            if (err.code === "auth/email-already-in-use") {
                return response
                    .status(400)
                    .json({ email: "This email has already been registered!" });
            } else {
                return response.status(500).json({
                    general: "Something went wrong, please try again!",
                });
            }
        });
};

exports.getUser = (request, response) => {
    let userData = {};
    db.doc(`/users/${request.user.email}`)
        .get()
        .then((doc) => {
            if (doc.exists) {
                userData.userCredentials = doc.data();
                return response.json(userData);
            }
        })
        .catch((error) => {
            console.error(error);
            return response.status(500).json({ error: error.code });
        });
};

exports.updateUser = (request, response) => {
    let document = db.collection("users").doc(`${request.user.email}`);
    document
        .update(request.body)
        .then(() => {
            response.json({ message: "User updated successfully!" });
        })
        .catch((error) => {
            console.error(error);
            return response.status(500).json({
                message: "Failed to update user!",
            });
        });
};
