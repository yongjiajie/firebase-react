const { db } = require("../util/admin");

exports.getAllBudgets = (request, response) => {
    db.collection("budgets")
        .where("email", "==", request.user.email)
        .get()
        .then((data) => {
            let budgets = [];
            data.forEach((doc) => {
                budgets.push({
                    id: doc.id,
                    name: doc.data().name,
                    accounts: doc.data().accounts,
                });
            });
            return response.json(budgets);
        })
        .catch((err) => {
            console.error(err);
            return response.status(500).json({ error: err.code });
        });
};

exports.getABudget = (request, response) => {
    const document = db.doc(`/budgets/${request.params.budgetId}`);
    document
        .get()
        .then((doc) => {
            if (!doc.exists) {
                return response
                    .status(404)
                    .json({ error: "Budget not found!" });
            }
            return response.json(doc);
        })
        .catch((err) => {
            console.error(err);
            return response.status(500).json({ error: err.code });
        });
};

exports.addABudget = (request, response) => {
    if (request.body.name.trim() === "") {
        return response.status(400).json({ name: "Must not be empty!" });
    }

    const newBudget = {
        name: request.body.name,
        createdAt: new Date(),
        email: request.user.email,
    };

    db.collection("budgets")
        .add(newBudget)
        .then((doc) => {
            const responseBudget = newBudget;
            responseBudget.id = doc.id;
            return response.json(responseBudget);
        })
        .catch((err) => {
            response.status(500).json({ error: "Something went wrong!" });
            console.error(err);
        });
};

exports.deleteABudget = (request, response) => {
    const document = db.doc(`/budgets/${request.params.budgetId}`);
    document
        .get()
        .then((doc) => {
            if (doc.data().email !== request.user.email) {
                return response
                    .status(403)
                    .json({ error: "You are unauthorized!" });
            }

            if (!doc.exists) {
                return response
                    .status(404)
                    .json({ error: "Budget not found!" });
            }
            return document.delete();
        })
        .then(() => {
            response.json({ message: "Budget successfully deleted!" });
        })
        .catch((err) => {
            console.error(err);
            return response.status(500).json({ error: err.code });
        });
};

exports.editABudget = (request, response) => {
    if (request.body.id || request.body.createdAt) {
        console.log(request.body.createdAt);
        return response.status(403).json({
            message: "You are not allowed to edit these fields in the budget!",
        });
    }

    let document = db.collection("budgets").doc(`${request.params.budgetId}`);
    document
        .update(request.body)
        .then(() => {
            response.json({ message: "Budget updated successfully!" });
        })
        .catch((err) => {
            console.error(err);
            return response.status(500).json({ error: err.code });
        });
};
