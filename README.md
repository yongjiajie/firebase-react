# Firebase

This project was created to learn about *Firebase*. This was created thanks to the amazing tutorial by Sharvin Shah at this [link](https://www.freecodecamp.org/news/how-to-build-a-todo-application-using-reactjs-and-firebase/). 

The following technologies are used:

- `express`
- `firebase-functions`

## Features

- Create an item
- Read an item
- Update an item
- Delete an item
- Register as a "user"
- Log in as a "user"
