const isEmpty = (string) => {
    if (string.trim() === "") {
        return true;
    }
    return false;
};

const isEmail = (email) => {
    const regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (email.match(regex)) {
        return true;
    }
    return false;
};

exports.validateLogin = (data) => {
    let errors = {};

    if (isEmpty(data.email)) {
        errors.email = "Must not be empty!";
    }

    if (isEmpty(data.password)) {
        errors.password = "Must not be empty!";
    }

    return {
        errors,
        valid: Object.keys(errors).length === 0 ? true : false,
    };
};

exports.validateSignUp = (data) => {
    let errors = {};

    if (isEmpty(data.email)) {
        errors.email = "Must not be empty!";
    } else if (!isEmail(data.email)) {
        errors.email = "Must be valid email address!";
    }

    if (isEmpty(data.password)) {
        errors.password = "Must not be empty!";
    }

    if (data.password !== data.confirmPassword) {
        errors.confirmPassword = "Passwords must be the same!";
    }

    return {
        errors,
        valid: Object.keys(errors).length === 0 ? true : false,
    };
};
